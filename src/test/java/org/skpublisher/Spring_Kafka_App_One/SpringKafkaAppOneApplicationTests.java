package org.skpublisher.Spring_Kafka_App_One;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringKafkaAppOneApplicationTests {

	@Test
	public void contextLoads() {
	}

}
