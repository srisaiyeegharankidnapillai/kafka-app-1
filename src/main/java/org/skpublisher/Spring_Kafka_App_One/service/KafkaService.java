package org.skpublisher.Spring_Kafka_App_One.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaService {
	
	String topicName = "application";
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	
	public String sendMessage(String msg) {
		kafkaTemplate.send(topicName, msg);
		return msg + " has been sent!";
		
	}

}
